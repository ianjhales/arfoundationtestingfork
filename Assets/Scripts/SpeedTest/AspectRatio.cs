using System;

namespace Mekamon.AR.Experiments.SpeedTest {

    [Serializable]
    public class AspectRatio {
        public int Horizontal;
        public int Vertical;

        public float Multiplier => (Vertical / (float) Horizontal);
    } 
}