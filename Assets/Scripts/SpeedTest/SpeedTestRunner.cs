using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OpenCVForUnity;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;
using UnityEngine.XR.ARExtensions;

namespace Mekamon.AR.Experiments.SpeedTest {
    public class SpeedTestRunner : MonoBehaviour {

        enum TestMode {
            ConversionResolution,
            RectResolution
        }
        
        
        [Tooltip("A list of resolutions we want to test, in terms of width. Height is set based on Aspect Ratio.")]
        [SerializeField]
        private TestMode _testMode;

        [Header("Input Data")]
        [Tooltip("A list of resolutions we want to test, in terms of width. Height is set based on Aspect Ratio.")]
        [SerializeField]
        private List<int> _widths;
        // Serialised list of desired output aspect-ratios
        [Tooltip("A list of aspect ratios to test.")] [SerializeField]
        private List<AspectRatio> _aspectRatios;

        [Tooltip("A list of aspect ratios to test.")] [SerializeField]
        private List<RectSize> _rectSizes;


        [Header("Display/Debugging")]
        [Tooltip("A text field to provide status updates.")] [SerializeField]
        private Text _statusText;
        
        [SerializeField] private RawImage _debugImage;
        
        // Get a list of all configurations
        private CameraConfigurationManager _cameraConfigurationManager;
        private ResolutionSpeedTestResultWriter<InputResolution> _resultsWriter;

        private Coroutine _runnerCoroutine;

        private Mat _testMat;
        private Texture2D _debugTexture;
        private float _deltaTime = 0.0f;

        private ImageGrabber _imageGrabber;

        private static string OUTPUT_DIRECTORY; 
        
        private void Awake() {
            OUTPUT_DIRECTORY = Application.persistentDataPath + "/speedTest";
            _cameraConfigurationManager = new CameraConfigurationManager();
            Directory.CreateDirectory(OUTPUT_DIRECTORY);
            _imageGrabber = new ImageGrabber();
            
        }

        public IEnumerator TestConversionOutputConfiguration(InputResolution input, List<IOutputSizeData> conversionResolutions) {
            foreach (var conversion in conversionResolutions) {
                UpdateStatusText(input, conversion);

                if (conversion.width > input.width || conversion.height > input.height) {
                    Debug.LogWarningFormat("Skipping invalid conversion size: {0} => {1}", input, conversion);
                    continue;
                }

                bool written = false;
                // Give FPS time to settle - grab 10 frames then read the FPS
                for (var f = 0; f < 30; ++f) {
                    Profiler.BeginSample("Grab Mat while settling");
                    _imageGrabber.GrabImageDirect(ref _testMat, conversion.width, conversion.height);
                    if (!written) {
                        Imgcodecs.imwrite(string.Format("{0}/test_image_{1}_{2}.png", OUTPUT_DIRECTORY, input, conversion),
                            _testMat);
                        written = true;
                    }
                    DebugDraw();
                    Profiler.EndSample();
                    yield return null;
                }

                // Take a bunch of FPS readings and average
                int numberOfReadings = 200;
                _deltaTime = 0.0f;
                float sumFPS = 0;
                for (var f = 0; f < numberOfReadings; ++f) {
                    Profiler.BeginSample("Grab Mat Reading");
                    _imageGrabber.GrabImageDirect(ref _testMat, conversion.width, conversion.height);
                    DebugDraw();
                    Profiler.EndSample();
                    sumFPS += ReadFPS();
                    yield return null;
                }

                // Record the average into the writer
                float averageFps = sumFPS / numberOfReadings;

                // TODO: Make the result writer take the rect size into account
                RecordFPS(input, conversion, averageFps);

                // Overwrite the results per-config in case we crash switching config... 
                _resultsWriter.WriteResults(OUTPUT_DIRECTORY + "/results.txt");
                
            }

            Debug.LogFormat("Completed test run for config: {0}", input);
        }

        public IEnumerator TestRectConfiguration(InputResolution input, List<RectSize> rectSizes) {
            foreach (var rectSize in rectSizes) {
                UpdateStatusText(input, rectSize);
                
                if (rectSize.width > input.width || rectSize.height > input.height) {
                    Debug.LogWarningFormat("Skipping invalid rectSize: {0} => {1}", input, rectSize);
                    yield break;
                }

                bool written = false;
                // Give FPS time to settle - grab 10 frames then read the FPS
                for (var f = 0; f < 30; ++f) {
                    Profiler.BeginSample("Grab Mat while settling");
                    _imageGrabber.GrabImageDirect(ref _testMat, rectSize.width, rectSize.height, true);
                    if (!written) {
                        Imgcodecs.imwrite(string.Format("{0}/test_rect_image_{1}_{2}.png", OUTPUT_DIRECTORY, input, rectSize),
                            _testMat);
                        written = true;
                    }
                    Profiler.EndSample();
                    yield return null;
                }

                // Take a bunch of FPS readings and average
                int numberOfReadings = 200;
                float sumFPS = 0;
                _deltaTime = 0.0f;
                for (var f = 0; f < numberOfReadings; ++f) {
                    Profiler.BeginSample("Grab Mat Reading");
                    _imageGrabber.GrabImageDirect(ref _testMat, rectSize.width, rectSize.height, true);
                    DebugDraw();
                    Profiler.EndSample();
                    sumFPS += ReadFPS();
                    yield return null;
                }

                // Record the average into the writer
                float averageFps = sumFPS / numberOfReadings;

                // TODO: Make the result writer take the rect size into account
                RecordFPS(input, rectSize, averageFps);

                // Overwrite the results per-config in case we crash switching config... 
                _resultsWriter.WriteResults(OUTPUT_DIRECTORY + "/rect_results.txt");
                
            }

            Debug.LogFormat("Completed test run for config: {0}", input);
        }

        private void DebugDraw() {
            if (_debugImage == null || _testMat == null || !_testMat.isContinuous()) {
                return;
            }

            if (_debugTexture == null || _debugTexture.width != _testMat.width() || _debugTexture.height != _testMat.height()) {
                _debugTexture = new Texture2D(_testMat.width(), _testMat.height(), TextureFormat.RGB24, false);
            }

            Utils.fastMatToTexture2D(_testMat, _debugTexture);
            _debugImage.texture = _debugTexture;
        }

        private void UpdateStatusText(InputResolution input = null, IOutputSizeData conversion = null) {
            if (input == null || conversion == null) {
                _statusText.text = "COMPLETE";
                return;
            }

            _statusText.text = string.Format("Input: {0}\nOutput: {1}", input, conversion);
        }

        private float ReadFPS() {
            _deltaTime += (Time.unscaledDeltaTime - _deltaTime) * 0.1f;
            return 1.0f / _deltaTime;
        }
        
        private bool RecordFPS(InputResolution input, IOutputSizeData outputSizeData, float averageFPS) {
            return _resultsWriter.CacheResult(input, outputSizeData, averageFPS);
        }

        public void StartTest() {
            StopTest();

            if (_testMode == TestMode.ConversionResolution) {
                _runnerCoroutine = StartCoroutine(RunOutputTest());
            }
            else {
                _runnerCoroutine = StartCoroutine(RunRectTest());
            }
        }

        public void StopTest() {
            if (_runnerCoroutine != null) {
                StopCoroutine(_runnerCoroutine);
            }
        }

        public IEnumerator RunOutputTest() {
            var inputResolutions = InputResolution.FromConfigurations(_cameraConfigurationManager.Configurations);
            var outputResolutions = ConversionResolution.FromWidthAndAspectRatio(_widths, _aspectRatios);

            _resultsWriter =
                new ResolutionSpeedTestResultWriter<InputResolution>(inputResolutions, outputResolutions);

            foreach (var config in _cameraConfigurationManager.Configurations) {
                _cameraConfigurationManager.CurrentConfiguration = config;
                // Give the switch time to happen and settle down
                yield return new WaitForSecondsRealtime(2f);

                yield return TestConversionOutputConfiguration(new InputResolution() {
                    width = config.width,
                    height = config.height
                }, outputResolutions);
            }

            UpdateStatusText();
        }

        public IEnumerator RunRectTest() {
            var inputResolutions = InputResolution.FromConfigurations(_cameraConfigurationManager.Configurations);
            var outputResolutions = ConversionResolution.FromWidthAndAspectRatio(_widths, _aspectRatios);

            var rectSizes = RectSize.NastyHack(_rectSizes);
            _resultsWriter = new ResolutionSpeedTestResultWriter<InputResolution>(inputResolutions, rectSizes);

            foreach (var config in _cameraConfigurationManager.Configurations) {
                _cameraConfigurationManager.CurrentConfiguration = config;
                // Give the switch time to happen and settle down
                yield return new WaitForSecondsRealtime(2f);

                yield return TestRectConfiguration(new InputResolution() {
                    width = config.width,
                    height = config.height
                }, _rectSizes);
            }

            UpdateStatusText();
        }
    }
}