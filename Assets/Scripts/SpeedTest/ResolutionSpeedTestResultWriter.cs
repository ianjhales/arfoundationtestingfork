using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Mekamon.AR.Experiments.SpeedTest {
    public class ResolutionSpeedTestResultWriter<InputType>  {

        private Dictionary<InputType, ResolutionTestResult<InputType>> _fullResultMatrix;
        private List<InputType> _inputResolutions;
        private List<IOutputSizeData> _outputResolutions;

        public ResolutionSpeedTestResultWriter(List<InputType> inputResolutions, List<IOutputSizeData> outputResolutions) {
            _inputResolutions = inputResolutions;
            _outputResolutions = outputResolutions;
            _fullResultMatrix = new Dictionary<InputType, ResolutionTestResult<InputType>>();
            
            foreach (var resolution in _inputResolutions) {
                _fullResultMatrix[resolution] = new ResolutionTestResult<InputType>(resolution, _outputResolutions);
            }
        }
        public bool WriteResults(string path) {
            List<string> rows = new List<string>();
            rows.Add(Headers());
            foreach (var results in _fullResultMatrix) {
                rows.Add(results.Value.ToRow());
            }

            try {
                File.WriteAllLines(path, rows);
                Debug.Log("File written to: " + path);
                return true;
            }
            catch (Exception ex) {
                Debug.LogError("Could not write output file:" + path );
                Debug.LogError(ex.Message);
            }

            return false;
        }

        public string Headers() {
            var keysStrings = _outputResolutions.Select(key => string.Format("\"{0}\"", key));
            return "\"Resolutions\"," + string.Join(",", keysStrings);
        }

        public bool CacheResult(InputType inputResolution, IOutputSizeData conversionResolution, float fps) {
            if (_fullResultMatrix.ContainsKey(inputResolution)) {
                return _fullResultMatrix[inputResolution].StoreResult(conversionResolution, fps);
            }

            Debug.LogError("Tried to add undeclared result into conversion res list: " + inputResolution);
            return false;
        }
        private class ResolutionTestResult<TInputType> {
            private readonly TInputType _inputResolution;
            private readonly Dictionary<IOutputSizeData, float> _fpsForResolution;

            public ResolutionTestResult(TInputType inputResolution, List<IOutputSizeData> resolutions) {
                _inputResolution = inputResolution;
                _fpsForResolution = new Dictionary<IOutputSizeData, float>();
                foreach (var resolution in resolutions) {
                    _fpsForResolution[resolution] = -1;
                }
            }

            public bool StoreResult(IOutputSizeData resolution, float fps) {
                if (!_fpsForResolution.ContainsKey(resolution)) {
                    Debug.LogError("Tried to add undeclared result into conversion res list: " + resolution);
                    return false;
                }

                _fpsForResolution[resolution] = fps;
                return true;
            }
            
            public string ToRow() {
                var resultsStrings = _fpsForResolution.Values.Select(value => string.Format("\"{0}\"", value));
                return string.Format("\"{0}\",", _inputResolution) + string.Join(",", resultsStrings);
            }
            
        }
    }
}