using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARExtensions;

namespace Mekamon.AR.Experiments.SpeedTest {

    public interface IOutputSizeData {
        string ToString();
        int width { get; }
        int height { get; }
    }
    
    public class CameraResolution : IOutputSizeData {

        public override string ToString() {
            return string.Format("{0} x {1}", width, height);
        }

        public int width { get; set;  }
        public int height { get; set; }

        public override bool Equals(object obj) {
            if (obj.GetType() != this.GetType()) {
                return false;
            }

            return width == (obj as CameraResolution).width && (obj as CameraResolution).height == height;
        }

        public override int GetHashCode() {
            return this.ToString().GetHashCode();
        }
    }

    public class InputResolution : CameraResolution {
        public static List<InputResolution> FromConfigurations(CameraConfigurationCollection configs) {
            List<InputResolution> resolutions = new List<InputResolution>(configs.count);
            foreach (var config in configs) {
                resolutions.Add(new InputResolution {
                    width = config.width,
                    height = config.height
                });
            }

            return resolutions;
        }
    }

    public class ConversionResolution : CameraResolution {
        public static List<IOutputSizeData> FromWidthAndAspectRatio(List<int> widths, List<AspectRatio> ratios) {
            List<IOutputSizeData> resolutions = new List<IOutputSizeData>(widths.Count * ratios.Count);
            foreach (var width in widths) {
                foreach (var ratio in ratios) {
                    var height = (int)(ratio.Multiplier * width);
                    resolutions.Add(new ConversionResolution {
                        width = width,
                        height = height
                    });
                }
            }

            return resolutions;
        }
    }
}