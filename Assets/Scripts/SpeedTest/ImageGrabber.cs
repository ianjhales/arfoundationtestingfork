using System;
using System.Collections.Generic;
using OpenCVForUnity;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;
using UnityEngine.XR.ARExtensions;
using UnityEngine.XR.ARFoundation;

namespace Mekamon.AR.Experiments {
    public class ImageGrabber {

        public ImageGrabber() {
            _texture2d = null;
        }
        
        private Texture2D _texture2d;
        private const TextureFormat format = TextureFormat.RGB24;
        
        public void GrabMat(ref Mat mat, int outputWidth, int outputHeight, bool useRect=false) {
            Profiler.BeginSample("Releasing the mat");
            if (mat != null) {
                mat.release();
            }
            Profiler.EndSample();
            
            Profiler.BeginSample("Grabbing image");
            bool success = GrabImage(outputWidth, outputHeight, useRect);
            Profiler.EndSample();
            
            if (success) {
                Profiler.BeginSample("Converting to Mat");
                TextureToMat(ref mat);
                Profiler.EndSample();
            }
            else {
                Debug.LogError("Failed to capture image frame");
            }
        }

        private NativeArray<byte> _textureBuffer;
        public unsafe bool GrabImageDirect(ref Mat mat, int outputWidth, int outputHeight, bool useRect=false) {
            CameraImage image;
            Profiler.BeginSample("Get image");
            if (!ARSubsystemManager.cameraSubsystem.TryGetLatestImage(out image)) {
                Profiler.EndSample();
                image.Dispose();
                return false;
            }
            Profiler.EndSample();

            if (_texture2d == null || _texture2d.width != outputWidth || _texture2d.height != outputHeight) {
                _texture2d = new Texture2D(outputWidth, outputHeight, format, false);
            }

            var rectOffset = new Vector2Int(0,0);
            var rectSize = new Vector2Int(image.width, image.height);

            if (useRect) {
                rectSize.x = outputWidth;
                rectSize.y = outputHeight;
                
                rectOffset.x = (image.width - rectSize.x) / 2;
                rectOffset.y = (image.height - rectSize.y) / 2;
            }
            
            // Convert the image to format, flipping the image across the Y axis.
            // We can also get a sub rectangle, but we'll get the full image here.
            var conversionParams = new CameraImageConversionParams
            {
                inputRect = new RectInt(rectOffset.x, rectOffset.y, rectSize.x, rectSize.y),
                outputDimensions = new Vector2Int(outputWidth, outputHeight),
                outputFormat = format
            };

            int size = image.GetConvertedDataSize(conversionParams);

            Profiler.BeginSample("Allocating Texture Buffer");
            if (_textureBuffer == null || size != _textureBuffer.Length) {
                _textureBuffer = new NativeArray<byte>(size, Allocator.Temp);
            }
            Profiler.EndSample();

            
            try {
                Profiler.BeginSample("Recreating RGB");
                RecreateMatIfSizeDifference(ref mat, outputWidth, outputHeight, CvType.CV_8UC3);
                Profiler.EndSample();
                
                IntPtr unsafePtr = new IntPtr(_textureBuffer.GetUnsafePtr());
                Profiler.BeginSample("Convert image");
                image.Convert(conversionParams, unsafePtr, _textureBuffer.Length);
                Profiler.EndSample();
                
                // Make doubly sure the pointer is still in the right place...
                unsafePtr = new IntPtr(_textureBuffer.GetUnsafePtr());
                Profiler.BeginSample("Copy to mat");
                Utils.copyToMat(unsafePtr, mat);
                Profiler.EndSample();
            }
            finally {
                // We must dispose of the CameraImage after we're finished
                // with it to avoid leaking native resources.
                image.Dispose();
            }

            return true;
        }
        public unsafe bool GrabImage(int outputWidth, int outputHeight, bool useRect) {
            CameraImage image;
            // it acquires a native resource that must be disposed (see below).
            Profiler.BeginSample("Get image");
            if (!ARSubsystemManager.cameraSubsystem.TryGetLatestImage(out image)) {
                Profiler.EndSample();
                image.Dispose();
                return false;
            }
            Profiler.EndSample();


            // Once we have a valid CameraImage, we can access the individual image "planes"
            // (the separate channels in the image). CameraImage.GetPlane provides
            // low-overhead access to this data. This could then be passed to a
            // computer vision algorithm. Here, we will convert the camera image
            // to an RGBA texture and draw it on the screen.

            // Choose an RGBA format.
            // See CameraImage.FormatSupported for a complete list of supported formats.

            if (_texture2d == null || _texture2d.width != outputWidth || _texture2d.height != outputHeight) {
                _texture2d = new Texture2D(outputWidth, outputHeight, format, false);
            }

            var rectOffset = new Vector2Int(0,0);
            var rectSize = new Vector2Int(image.width, image.height);

            if (useRect) {
                rectSize.x = outputWidth;
                rectSize.y = outputHeight;
                
                rectOffset.x = (image.width - rectSize.x) / 2;
                rectOffset.y = (image.height - rectSize.y) / 2;
            }
            
            // Convert the image to format, flipping the image across the Y axis.
            // We can also get a sub rectangle, but we'll get the full image here.
            var conversionParams = new CameraImageConversionParams
            {
                inputRect = new RectInt(rectOffset.x, rectOffset.y, rectSize.x, rectSize.y),
                outputDimensions = new Vector2Int(outputWidth, outputHeight),
                outputFormat = format
            };

            // Texture2D allows us write directly to the raw texture data
            // This allows us to do the conversion in-place without making any copies.
            Profiler.BeginSample("Read raw data");
            var rawTextureData = _texture2d.GetRawTextureData<byte>();
            Profiler.EndSample();
            try {
                Profiler.BeginSample("Convert image");
                image.Convert(conversionParams, new IntPtr(rawTextureData.GetUnsafePtr()), rawTextureData.Length);
                Profiler.EndSample();
            }
            finally {
                // We must dispose of the CameraImage after we're finished
                // with it to avoid leaking native resources.
                image.Dispose();
            }

            // Apply the updated texture data to our texture
//            Profiler.BeginSample("Apply texture");
//            _texture2d.Apply();
//            Profiler.EndSample();
            return true;
        }

        private void TextureToMat(ref Mat mat) {
            Profiler.BeginSample("Recreating RGB");
            RecreateMatIfSizeDifference(ref mat, CvType.CV_8UC3);
            Profiler.EndSample();            
            
            Profiler.BeginSample("Texture To Mat");
            Utils.fastTexture2DToMat(_texture2d, mat, false);
            Profiler.EndSample();            
            Profiler.BeginSample("RGB to BGR");
            Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2BGR);
            Profiler.EndSample();
        }

        private void RecreateMatIfSizeDifference(ref Mat mat, int type) {
            RecreateMatIfSizeDifference(ref mat, _texture2d.width, _texture2d.height, type);
        }

        private void RecreateMatIfSizeDifference(ref Mat mat, int width, int height, int type) {
            
            if (mat == null ||  height != mat.height() || width != mat.width())
            {
                if (mat != null) {
                    mat.release();
                }
                mat = new Mat(_texture2d.height, _texture2d.width, type);
            }
        }

        public unsafe void veryFastTexture2DToMat (ref Mat mat)
        {

            Profiler.BeginSample("veryFastTexture2DToMat: PreAmble");
            if (mat != null)
                mat.ThrowIfDisposed ();

            if (mat == null)
                throw new ArgumentNullException ("mat == null");
            Profiler.EndSample();

            Profiler.BeginSample("veryFastTexture2DToMat: GetRawTextureData");
//            IntPtr texturePtr = _texture2d.GetNativeTexturePtr();
            NativeArray<byte> _textureBytes = _texture2d.GetRawTextureData<byte>();
            IntPtr ptr = new IntPtr(_textureBytes.GetUnsafePtr());
            Profiler.EndSample();

            Profiler.BeginSample("veryFastTexture2DToMat: PUT");
//            mat.put (0, 0, _textureBytes);
            Utils.copyToMat(ptr, mat);
            Profiler.EndSample();
        }
        
 

        // THIS IS ENTIRELY BOLLOCKS AND DOESN'T WORK AT-FUCKING-ALL...
//        public unsafe bool GrabImagePlanes(ref Mat mat, int width, int height) {
//            CameraImage image;
//            // it acquires a native resource that must be disposed (see below).
//            Profiler.BeginSample("Get image");
//            if (!ARSubsystemManager.cameraSubsystem.TryGetLatestImage(out image)) {
//                image.Dispose();
//                Profiler.EndSample();
//                return false;
//            }
//           Profiler.EndSample();
//           
//            try {
//                if (mat == null || image.height != mat.height() || image.width != mat.width()) {
//                    if (mat != null) {
//                        mat.release();
//                    }
//
//                    mat = new Mat(image.height, image.width, CvType.CV_8UC3);
//                }
//                
//                List<Mat> channels = new List<Mat>(image.planeCount);
//                Texture2D channelTexture;
//
//                CameraImagePlane yPlane = image.GetPlane(0);
//                CameraImagePlane uPlane = image.GetPlane(1);
//                CameraImagePlane vPlane = image.GetPlane(2);
//                
//                Mat yMat = new Mat(image.height, yPlane.rowStride / yPlane.pixelStride, CvType.CV_8U, Scalar.all(0));
//                Mat uMat = new Mat(image.height, uPlane.rowStride / yPlane.pixelStride,CvType.CV_8U, Scalar.all(0));
//                Mat vMat = new Mat(image.height, vPlane.rowStride / yPlane.pixelStride, CvType.CV_8U,Scalar.all(0));
//
//                var combinedBytes = new Byte[yPlane.data.Length + uPlane.data.Length + vPlane.data.Length];
//                Buffer.BlockCopy(yPlane.data.ToArray(),0, combinedBytes, 0, yPlane.data.Length);
//                Buffer.BlockCopy(vPlane.data.ToArray(), 0, combinedBytes, yPlane.data.Length, vPlane.data.Length);
//                Buffer.BlockCopy(uPlane.data.ToArray(), 0, combinedBytes, yPlane.data.Length + vPlane.data.Length, uPlane.data.Length);
//                
//                
//                yuv = new Mat((int)(image.height*1.5), image.width, CvType.CV_8UC1);
//                yuv.put(0, 0, combinedBytes);
//                
//                Profiler.BeginSample("Convert YUV to BGR");
//                Imgproc.cvtColor(yuv, mat, Imgproc.COLOR_YUV2BGR_I420);
//                Profiler.EndSample();
//                Imgcodecs.imwrite(Application.persistentDataPath + "/speedTest/output_I420.png", mat);
//                Imgproc.cvtColor(yuv, mat, Imgproc.COLOR_YUV2BGR_NV21);
//                Imgcodecs.imwrite(Application.persistentDataPath + "/speedTest/output_NV21.png", mat);
//
//                
////                
////                yMat.put(0, 0, yPlane.data.ToArray());
////                uMat.put(0, 0, uPlane.data.ToArray());
////                vMat.put(0, 0, vPlane.data.ToArray());
////
////                Imgcodecs.imwrite(Application.persistentDataPath + "/speedTest/ychannel1.png", yMat);
////                Imgcodecs.imwrite(Application.persistentDataPath + "/speedTest/uchannel1.png", uMat);
////                Imgcodecs.imwrite(Application.persistentDataPath + "/speedTest/vchannel1.png", vMat);
////                
////                channels.Add(yMat);
////                channels.Add(uMat);
////                channels.Add(vMat);
//
//                
//                Profiler.BeginSample("Downscale");
//                Imgproc.resize(mat, mat, new Size(width, height)); // TODO: This may be done earlier?
//                Profiler.EndSample();
//
//                return true;
//            }
//            finally {
//                image.Dispose();
//            }
//        }
//               
    }
}