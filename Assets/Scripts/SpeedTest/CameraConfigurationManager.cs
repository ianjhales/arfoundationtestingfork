using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.UI;
using UnityEngine.XR.ARExtensions;
using UnityEngine.XR.ARFoundation;

namespace Mekamon.AR.Experiments.SpeedTest {
    public class CameraConfigurationManager {
        public CameraConfigurationManager() {
            _cameraSubsystem = ARSubsystemManager.cameraSubsystem;
        }

        private XRCameraSubsystem _cameraSubsystem;

        public XRCameraSubsystem CameraSubsystem {
            get {
                // Could happen if the class was instantiated before the session was ready
                if (_cameraSubsystem == null) {
                    _cameraSubsystem = ARSubsystemManager.cameraSubsystem;
                }

                return _cameraSubsystem;
            }
        }


        private CameraConfigurationCollection _configurations;

        public CameraConfigurationCollection Configurations {
            get {
                // Bit hacky, but this will throw if we've never tried to get the number of configs
                try {
                    int numConfigs = _configurations.count;
                    if (numConfigs == 0) {
                        throw new InvalidOperationException("No available configs");
                    }
                }
                catch (InvalidOperationException ex) {
                    if (!TryLoad()) {
                        Debug.LogWarning("Could not load configurations - is something wrong or just unsupported?");
                    }
                }

                return _configurations;
            }
        }

        private List<string> _configurationNames;
        public List<string> ConfigurationNames => _configurationNames;

        public bool TryLoad() {
            if (_cameraSubsystem == null)
                return false;

            // No configurations available probably means this feature
            // isn't supported by the current device.
            _configurations = _cameraSubsystem.Configurations();
            if (_configurations.count == 0)
                return false;

            _configurationNames = new List<string>(_configurations.count);
            foreach (var config in _configurations)
                _configurationNames.Add(config.ToString());

            return true;
        }

        public void SetConfigById(int configurationIndex) {
            var cameraSubsystem = ARSubsystemManager.cameraSubsystem;
            // Check that the value makes sense

            if (configurationIndex >= Configurations.count)
                return;

            // Get that configuration by index
            var configuration = Configurations[configurationIndex];

            // Make it the active one
            cameraSubsystem.SetCurrentConfiguration(configuration);
        }

        public CameraConfiguration CurrentConfiguration {
            get { return CameraSubsystem.GetCurrentConfiguration(); }
            set { CameraSubsystem.SetCurrentConfiguration(value); }
        }
    }
}