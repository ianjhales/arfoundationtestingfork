using System;
using System.Collections.Generic;
using UnityEngine;

namespace Mekamon.AR.Experiments.SpeedTest {
    [Serializable]
    public class RectSize : IOutputSizeData {
        public Vector2Int size;

        public int width {
            get { return size.x; }
        }

        public int height {
            get { return size.y; }
        }
        public override string ToString() {
            return string.Format("{0} x {1}", width, height);
        }

        // Fuck this to hell
        public static List<IOutputSizeData> NastyHack(List<RectSize> rectSizes) {
            List<IOutputSizeData> lst = new List<IOutputSizeData>();
            foreach (var r in rectSizes) {
                lst.Add(r);
            }

            return lst;
        }
    }
}