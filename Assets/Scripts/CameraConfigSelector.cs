using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.UI;
using UnityEngine.XR.ARExtensions;

namespace DefaultNamespace {
    public class CameraConfigSelector {

        public enum SelectionCriteria {
            SmallestFitting
            // TODO:IJH: Potentially other selection criteria
        }

        private XRCameraSubsystem _cameraSubsystem;
        private CameraConfiguration currentlyChosenConfig;

        public CameraConfiguration CurrentlyChosenConfig => currentlyChosenConfig;

        public CameraConfigSelector(XRCameraSubsystem cameraSubsystem) {
            _cameraSubsystem = cameraSubsystem;
        }

        // TODO: having to pass in the width and height here seems messy, but it's not worth doing something more
        // elegant in the short term
        public void Select(SelectionCriteria criteria, int width, int height) {
            if (criteria == SelectionCriteria.SmallestFitting) {
                currentlyChosenConfig = SmallestPossibleForVideoSize(width, height);
            }

            _cameraSubsystem.SetCurrentConfiguration(currentlyChosenConfig);
        }

        // AFAICT there is no guaranteed ordering to the configs, although empirically they *seem* to come through from
        // smallest to largest on Android and vice versa on iOS, but I don't want to rely on that...
        private CameraConfiguration SmallestPossibleForVideoSize(int width, int height) {

            var configs = _cameraSubsystem.Configurations();
            var desiredArea = width * height;

            // If we haven't got one big enough, we want to pick the largest. Otherwise pick the one that best fits.
            int bestConfig = -1;
            int bestConfigArea = 0;

            bool haveLargeEnoughConfig = false;

            for (var c = 0; c < configs.count; ++c) {
                var config = configs[c];
                var widthDiff = config.width - width;
                var heightDiff = config.height - height;

                var area = config.width * config.height;

                if (widthDiff >= 0 && heightDiff >= 0) {
                    // This is the first config big enough for us to use - set it as best
                    // If we've seen other valid ones, but this one is smaller, pick it instead
                    if (!haveLargeEnoughConfig || area < bestConfigArea) {
                        haveLargeEnoughConfig = true;
                        bestConfigArea = area;
                        bestConfig = c;
                    }
                }

                // Only need to do find biggest if we don't have a big enough config already, otherwise it won't get used.
                if (haveLargeEnoughConfig || area < bestConfigArea) {
                    continue;
                }

                bestConfigArea = area;
                bestConfig = c;

            }

            return configs[bestConfig];
        }
    }
}