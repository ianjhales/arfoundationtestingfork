﻿using System.Collections.Generic;
using Mekamon.AR.Experiments.SpeedTest;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARExtensions;
using UnityEngine.XR.ARFoundation;

/// <summary>
/// Populates a drop down UI element with all the supported
/// camera configurations and changes the active camera
/// configuration when the user changes the selection in the dropdown.
/// 
/// The camera configuration affects the resolution (and possibly framerate)
/// of the hardware camera during an AR session.
/// </summary>
[RequireComponent(typeof(Dropdown))]
public class CameraConfigController : MonoBehaviour {
    private CameraConfigurationManager _cameraConfigurationManager;
    Dropdown m_Dropdown;

    void Awake()
    {
        m_Dropdown = GetComponent<Dropdown>();
        m_Dropdown.ClearOptions();
        _cameraConfigurationManager = new CameraConfigurationManager();
        _cameraConfigurationManager.TryLoad();
    }

    void PopulateDropdown() {
        var cameraConfigs = _cameraConfigurationManager.Configurations;
        m_Dropdown.AddOptions(_cameraConfigurationManager.ConfigurationNames);

        // 2. Use a normal for...loop
        var currentConfig = _cameraConfigurationManager.CurrentConfiguration;
        for (int i = 0; i < cameraConfigs.count; i++)
        {
            // Find the current configuration and update the drop down value
            if (currentConfig == cameraConfigs[i])
                m_Dropdown.value = i;
        }
    }

    void Update()
    {
        if (_cameraConfigurationManager.ConfigurationNames.Count == 0)
            PopulateDropdown();
    }
}
