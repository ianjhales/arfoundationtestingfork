using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARExtensions;
using UnityEngine.XR.ARFoundation;

namespace DefaultNamespace {
    public class ARDropdownController : MonoBehaviour {

        List<string> m_ConfigurationNames;
        Dropdown m_Dropdown;

        void Awake()
        {
            m_Dropdown = GetComponent<Dropdown>();
            m_Dropdown.ClearOptions();
            m_ConfigurationNames = new List<string>();
        }

        void PopulateDropdown()
        {
            var cameraSubsystem = ARSubsystemManager.cameraSubsystem;
            if (cameraSubsystem == null)
                return;
            
            m_ConfigurationNames.Add("4:3");
            m_ConfigurationNames.Add("16:9");
            m_ConfigurationNames.Add("16:10");
            
            m_Dropdown.AddOptions(m_ConfigurationNames);
        }

        void Update()
        {
            if (m_ConfigurationNames.Count == 0)
                PopulateDropdown();
        }

        
    }
}