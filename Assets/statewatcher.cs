﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARFoundation;

public class statewatcher : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
            ARSubsystemManager.systemStateChanged += ArSubsystemManagerOnSystemStateChanged;
    }

    private void ArSubsystemManagerOnSystemStateChanged(ARSystemStateChangedEventArgs obj)
    {
        
        Debug.Log(obj.state);
    }

}
